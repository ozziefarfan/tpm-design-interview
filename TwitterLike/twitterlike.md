# Application: Twitter Like
_The goal is to design an_app like web service, where users can do something for some specific reason._
The goal of this app is to allow users to post and interact with short messages known as "tweets". Registered users can post, like, and retweet tweets, but unregistered users can only read those that are publicly available.

## 1. What is an_app
_This app does the following (think problem it solves Why => What => How):_
* Twitter is a service for friends, family, and coworkers to communicate and stay connected through the exchange of quick, frequent messages. 
* People post Tweets, which may contain photos, videos, links, and text. 
* These messages are posted to your profile, sent to your followers, and are searchable on Twitter search.  

## 2. Defining Scope
### 2.1 Assumptions
_Write down all the assumptions that are being made for this document_

* The scope of this document is posting of twitts and follow up other users.

<details><summary>Click to expand examples</summary>

* The system will only allow users to upload files < 10 Mb
* Registration/Account creation process will not be discussed
* We expect 1M new objects pasted every day
* This is a read heavy system  1:5 proportion (write:read)
* We will focus on the "Write" (put) scenario
* Offline use cases are not supported at this moment

</details>

### 2.2 Users and Interactions
_Write down all the type of users this app will have and their interactions with it (use cases at high level)_

* Content owners who are generating tweets and following others.
* Famous content users that post tweets and have millions of followers.
* Readers only.  Not registered users that can only read tweets.
* Other systems.  Not applicable.


### 2.3 User Requirements
_From the user / product owner point of view, what are the requirements the solution should meet?_

* Content owners need to be able to post tweets (text, images, links, files)
* Registered users should be able to re-tweet other's tweets 
* Registered users should be able to follow other users/tweets
* Users should be able to search tweets

<details><summary>Click to expand examples</summary>

* After users register for the service, they will be granted access to load text and files.
* Users will be able to generate a unique URL to share with others
* Users will be able to generate a custom URL to share with others
* The URL generated has to be unique and not predictable
* Users should be able to change the expiration date of their content
* No single user can have more than 10 Gb uploaded at a given time

</details>


### 2.4 Functional Requirements
_Write down what functionality is expected from the solution being discussed_

* System needs to have a registration feature for profile management
* System must support various formats for twitts posts
* Users must be able to follow or unfollow other tweets
* Users must be able to re-tweet somebody else's tweet
* Users must be able to perform searches
* System should treat differently registered and unregistered (read-only) users 
* Tweets are limited to 140 characters.  Long URLs should be shorten.

<details><summary>Click to expand examples</summary>

* System needs to have a registration feature for profile management
* System must support files in various formats.  
* Files must be accessible to anybody with the access URL (no registration required)
* Every generated URL must be unique
* URLs and content should expire after 15 days
* The service must provide insights and analytics through a dashboard
* The system should capture telemetry for users, usage, and content
* Users should be able to upload files/photos from any device.
* Users should be able to share files/folders with other users.
* Support automatic synchronization between devices
* Support large files up to 2 GBs
* Support to offline editing
* Support data snapshots so that users can go back to a given version.

</details>


### 2.5 Non Functional Requirements
_Write down non functional requirements that the solution should meet_

* The system is heavy ready and should handle these loads without increasing latency
* High Availability
* Posting should be done fast 
* Rendering should be done fast
* Notification of new postings can be dealyed a couple of seconds.
* Scalable. There are about 5k tweets per second.

<details><summary>Click to expand examples</summary>

* The system should be highly available so that users can access their content anytime.
* The system should be highly reliable (uploaded content should not be lost)
* The system should meet low latency requirements so that interaction with users is highly responsive

</details>

### 2.6 Operating Environments
_What are the the target platforms and/or operating environments_
* Web browsers on desktop
* Web browsers on mobile devices (phones / tablets)
* Custom hardware (??)
* Online vs Offline - Does it need to be handled? Not for this example.

### 2.7 Design Considerations
_What are some of the considerations must be kept in mind while designing this solution_
* The solution should use microservices
* There must be a REST api service to allow the same basic functions: upload, download, list content
* 

### 2.8 Capacity Considerations
_Identify size of data that the app will be handling.  This is an estimation based on the initial assumptions. Below is a common pattern_

* If we have 15k tweets per second, and we assume 100 characters per tweet (text) average.  We will need 
   100 x 15k tweets x 24 x 3600 = 129 Gb daily
   if we want to use only 70% of capacity then we need => 129 / 70% => 185 Gb

<details><summary>Click to expand examples</summary>

**From Assumptions**:
*  Write:Read ratio - 1:5 
*  1M new objects per day
*  objects are in average 10Kb in size

**Calulations**:
* Write objects per second:  1M/(24 hours x 3600 sec) ~= 12 pastes per second
* Read objects per second:   5 x Write ~= 60 reads per second
* Capacity storage per day:  1M x 10kb = 10Gb per day
* Capacity storage for a year:  10Gb x 365 days = 3.6TB
* Storage for 10 years: 36TB
* Increase capacity to use up to 70% only:  36TB/.7 = 51.4TB

**Unique Keys**:
* We need unique keys for 36 Billion pastes in 10 years.
* If we use a 6 character key with base 64 encoding: 64^6 = 68.7 billion keys
* each key will be 6 bytes.  For 36 billion keys we need: 36B keys x 6 chars ~= 22GB (keys for 10 years)

**Cache**:
* Most used resources / keys will be cached.  Using 80/20 rule, where 20% of the objects will cause 80% of the traffic
* Total memory for cache:  5M reads x 10Kb x 20% ~= 10GB

</details>


### 3. Other Requirements
#### 3.1 Performance and Scalability
What Is the performance requirement (Low Latency, TPS, availability P99, etc.)
* Response time latency < 10ms

#### 3.2 Security
Security requirements.  Encryption / hashing / encoding / tokenization 
* Encryption for password and othe personal information. 

#### 3.3 Authentication and Authorization
Login / profiles  and access controls
* Login required to follow or post.  

#### 3.4 Software Quality
What is the software quality requirement and how to measure it.  For example, bugs per 1000 lines, test results, integration, etc.

#### 3.5 Telemetry / Logging / Analytics
Requirements to measure user and system activities, event logging, and analytics
* Analytics are required for users, topics, popularity, and all system telemetry

#### 3.6 Fault Tolerance
What is the expected fault tolerance? e.g. what happens if there is a crash in the middle of transaction
* Data is never lost 
* If failure during upload / download the operation will need to be repeated
* If the system crashes no Tweets should be lost (we will use redundancy)

#### 3.7 Availability
What is the expected availability.  How are outages going to be handled.
* 24x7 - highly available 

#### 3.8 Monitoring and Alerting
* All systems logs go to DataDog
* All application logs go to Splunk
* Alerting will be handle through PagerDuty and Slack

#### 3.9 Database Storage / Partitioning | Sharding / Replication / Retention Policy 
##### 3.9.1 Storage
* We will store billions of records
* We store 2 types of data:  object and metadata
* Objects are less than 10 Mb
* Metatada is less than 1 K
* There is no relationship between records (except for user --> Paste-bins)
* 10 TB
##### 3.9.2 Replication
* Data should be replicated and available on multiple regions
##### 3.9.3 Retention Policy
* Data will be available for 10 days
* Users can make data availabel for a custom number of days between 1-30 days
##### 3.9.4 Partitioning | Sharding
* Data will be partitioned using userid  

### 4. High Level Design
_Add a high level design diagram. Indicate at the high level how the solution will look like_

###### Very High Level Design

<img src="./image-2.png" alt="high level"
	title="Very High Level Design" width="700" />

###### Scenario:  User Login

<img src="./image.png" alt="high level"
	title="High Level Design" width="700" />

When the user logs in the request is passed to the "TweetFeed Service" that will contact the "User Profile Service" to get all the different channels the user follows.  With that information, the TweetFeed service will retrieve and send back the appropriate tweets back to the UI.  

### 5. Component Design
_Add a component design diagram and explain how workflows (use cases) will be fulfilled_

Going deeper into the solution we need to look at various components and how the data will flow through the system.

In this case...
<img src="./image-1.png" alt="Component level"
	title="Component Level Design" width="900" />

##### Brief explanation of diagram:
- The user sends a tweet
- The App Server generates an event with the tweet
- The tweet is picked up by the tweet-feed service. It stores the tweet and the metadata associated to it. If the tweet is an image it is stored in the blob storage.  If the Tweet contains a URL it will be shorten.
- All the metadata for the image object (location, size, time, etc.), and the short URL will be stored on the database.
- Then an event is sent to indicate that the event has been saved. Also, all the information is included in the message.
- The Feed-Update Service will see the new Tweet and using the Graph Service will identify who needs to be updated. 
- The Timeline Service will update the timeline for all users as it sees new events coming through.
- The Key Generation Service will work on the background and maintain pre-generated / cache keys for usage

#### 5.1 Technology Details
- Web server and other points of access will have load balancers (LB) 
- All the services will be run on Kubernetes for scaling

### 6. Faiure Modes / Risks
_What are the risks and potential issues to this design that need to be reviewed or solved. What are the conditions that will cause failures to the solution/system, and other risks identified._

- Caching keys can cause loss of unused keys if the system is restarted
- The Key generation service can be improved to avoid single point of failure
- RISKS / SOURCE OF FAILURES
- Service not Available
- Crash in the middle of transactions
- Circular links (e.g. web crawler)

### 7. External Interfaces
_Describe any requirements to external interfaces that the solution needs to interact with_

- Hardware Interfaces
- Software Interfaces

### 8. Database Design
_Add the database design, Entity Relationship Diagram, Event formats, data types, and any detail you have_

### 9. USER / SYSTEM WORKFLOW DIAGRAMS
_Start describing individual Workflows for users, and inside the system.  Use UML diagrams and other tools that will help you make this very clear _

### 10. COMPONENTS / API (Brainstorm)
_In this section do a deeper dive into the components. Define the service, why it is needed, what it does, and how it will accomplish this function. Also add 
API definitions and contracts if you have them_

### 11. OPS READINESS / MONITORING
_Ready to go to production? describe your support plan including things as Support channels / pager duty / Prod monitoring / SLA_

