# TPM Design Interview

Examples of Design for the Technical Program Manager interview.  The examples found here are provide a good framework for 
you to study and prepare for your TPM role.

* [Approach](Approach.md)
* [Initial Template](InitialTemplate.md)  This document is the initial template to start creating design documents. Although it is extensive, there are many sections missing that will need to be added as the design gets down to the specific component details.

#### Examples:

* [TinyURL](./TinyURL/tinyurl.md)
* [Paste Bin](./Web%20Bin/pastebin.md)
* [Uber](./UberLike/uberlike.md)
* [Instagram](instagr)


