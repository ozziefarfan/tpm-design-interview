# Approach

###

As you probably already read on many websites, "you don’t need to know everything! Your final design is not as important as the thought process 
behind your design choices. After all, this reflects the experience of actually working at a company. Engineers have a tremendous amount of freedom; we aren’t asked to implement fully-specced features, but rather to take ownership of open-ended problems and come up with the best solutions." [source](https://blog.pramp.com/how-to-succeed-in-a-system-design-interview-27b35de0df26)

You should cover the following steps:
1. Understand the Goals
2. Establish the Scope (What is IN, and what is OUT)
3. Design for the Right Scale
4. Start High Level ... then Drill-Down
5. Data Structures
6. Process Flows
7. Tradeoffs
