# Application: UberLik
The goal is to design an Uber like web service, where users can request a ride from their location to a given destination.

## 1. What is an_app
_This app does the following (think problem it solves Why => What => How):_
* Users can request a safe ride to their destination.
* It is safer than regular taxis as the service contains pre-registered drivers and users know who will pick them up.
* The cost of the ride is managed by the app so there is no money exchange between the driver an the user.
* It provides real-time updates regarding the driver, location, and time to pick up.

## 2. Defining Scope
### 2.1 Assumptions
_Write down all the assumptions that are being made for this document_
* The solution will discuss the workflow for requesting a ride and getting a driver assigned
* We expect to have 1M requests per day
* Offline use cases are not supported at this moment


### 2.2 Users and Interactions
_Write down all the type of users this app will have and their interactions with it (use cases at high level)_
* Riders - Users that request a ride. This users will log in to the app and request a ride from their location to a given destination.
* Drivers - This users will connect to the app to provide status (e.g. active/location), and will use the app to assign rides to themselves.
* Other systems.  There will be no REST API for this service.

### 2.3 User Requirements
_From the user / product owner point of view, what are the requirements the solution should meet?_

* Users should be able to see the location of drivers available in the area.
* Users need to be able to request a ride.
* users will be able to see the driver location.
* The solution should assign the driver based on parameters such as vehicle size, distance, etc.
* Drivers should be able to assign a ride to them once it is offered.
* The app should provide the cost of the ride up front. Before the request is submitted.
* Drivers should be able to give up a ride already assigned. This could be due to a personal problem, mechanical issues, etc.


### 2.4 Functional Requirements
_Write down what functionality is expected from the solution being discussed_

* System needs to have a registration feature for profile management: drivers and riders
* System needs to show location of drivers/cabs.
* System needs to assign drivers to requests. If there are multiple drivers the ride request will go to the nearest "Top 3" drivers. 
* Drivers need to have an easy way to assign/cancel a ride to themselves. 
* In the case of cancellation the use will be informed and the request should be re-submitted automatically.
* The user needs to be able to cancel a request.
* The user should receive details of the driver/vehicle that was assigned to the trip.  And the ETA.
* The service must provide insights and analytics through a dashboard
* The system should capture telemetry for users, drivers, usage, and others


### 2.5 Non Functional Requirements
_Write down non functional requirements that the solution should meet_
* The system should be highly available so that users can access their content anytime.
* The system should be highly reliable (uploaded content should not be lost)
* The system should meet low latency requirements so that interaction with users is highly responsive

### 2.6 Operating Environments
_What are the the target platforms and/or operating environments_
* Web browsers on desktop
* Web browsers on mobile devices (phones / tablets)
* Custom hardware - N/A
* Online vs Offline - Only online use cases will be supported for v1 POC

### 2.7 Design Considerations
_What are some of the considerations must be kept in mind while designing this solution_
* The solution should use microservices
* City maps and mapping services will use Google Maps APIs
* This is a high usage solution, so traffic is expected to be high, specially during events and other circumstances (e.g. peak hours, heavy weather, etc)
*  

### 2.8 Capacity Considerations
_Identify size of data that the app will be handling.  This is an estimation based on the initial assumptions. Below is a common pattern_

* Requests: 1M per day
* Vehicle position: updates every 4 seconds
* Requests per second: 12 rides per second

<details><summary>Click to expand examples</summary>

**From Assumptions**:
*  Write:Read ratio - 1:5 
*  1M new objects per day
*  objects are in average 10Kb in size

**Calulations**:
* Write objects per second:  1M/(24 hours x 3600 sec) ~= 12 pastes per second
* Read objects per second:   5 x Write ~= 60 reads per second
* Capacity storage per day:  1M x 10kb = 10Gb per day
* Capacity storage for a year:  10Gb x 365 days = 3.6TB
* Storage for 10 years: 36TB
* Increase capacity to use up to 70% only:  36TB/.7 = 51.4TB

**Unique Keys**:
* We need unique keys for 36 Billion pastes in 10 years.
* If we use a 6 character key with base 64 encoding: 64^6 = 68.7 billion keys
* each key will be 6 bytes.  For 36 billion keys we need: 36B keys x 6 chars ~= 22GB (keys for 10 years)

**Cache**:
* Most used resources / keys will be cached.  Using 80/20 rule, where 20% of the objects will cause 80% of the traffic
* Total memory for cache:  5M reads x 10Kb x 20% ~= 10GB

</details>


### 3. Other Requirements
#### 3.1 Performance and Scalability
What Is the performance requirement (Low Latency, TPS, availability P99, etc.)
* Response time latency < 10ms

#### 3.2 Security
Security requirements.  Encryption / hashing / encoding / tokenization 
* Passwords and personal data needs to be properly encrypted (PII data)
* Credit Card information, etc. needs to be protected

#### 3.3 Authentication and Authorization
Login / profiles  and access controls
* Only registered users can access the solution

#### 3.4 Software Quality
What is the software quality requirement and how to measure it.  For example, bugs per 1000 lines, test results, integration, etc.

#### 3.5 Telemetry / Logging / Analytics
Requirements to measure user and system activities, event logging, and analytics
* Telemetry for system, aaplication, users, click-streams, and others must be tracked
* Dashboard available for analytics

#### 3.6 Fault Tolerance
What is the expected fault tolerance? e.g. what happens if there is a crash in the middle of transaction
* Data is never lost 
* If failure during a ride the system and users will need to be updated immediately after it recovers

#### 3.7 Availability
What is the expected availability.  How are outages going to be handled.
* 24x7 - highly available 

#### 3.8 Monitoring and Alerting
* All systems logs go to DataDog
* All application logs go to Splunk
* Alerting will be handle through PagerDuty and Slack

#### 3.9 Database Storage / Partitioning | Sharding / Replication / Retention Policy 

##### 3.9.1 Storage
* We will store billions of records
* We users, drivers, trips, maps, etc.
* Metatada is less than 1K per user
* There is none or minimum relationship between records
* 10 TB

##### 3.9.2 Replication
* Data should be replicated and available on multiple regions

##### 3.9.3 Retention Policy
* Data will be available for 30 days and moved to S3 storage after that
* Trip logs will be made available for later analysis and updating maps / routes

##### 3.9.4 Partitioning | Sharding
* Data will be partitioned using city + area_code  

### 4. High Level Design
_Add a high level design diagram. Indicate at the high level how the solution will look like_

In this case, instead of creating a high-level design, let's use one of the simplest use cases to show some of the basic components. The following diagram shows what the solution would look like to address the scenario when the user logs in and wants to see vehicles available in the area.
The diagram below shows the following main workflows at the very-high-level : registration, requeste a cab, assign a ride, show cab, and analytics.

<img src="./image-2.png" alt="high level"
	title="High Level Design" width="900" />

<hr>

<img src="./image.png" alt="high level"
	title="High Level Design" width="800" />

##### Brief explanation of diagram:
The assumption is that the user will talk to some appserver that will..
* The user will get_cabs for a given location (long/lattitude)
* That location is translated to X,Y coordinates using Google Maps API
* Those coordinates are sent to the cab_mapping service that in turns talks to the cab_tracking service
* Once the information is collected, the data is sent back to the app
* The map will be updated
* To maintain the map with updated vehicles we will keep the customer connected with a Long Poll connection

### 5. Component Design
_Add a component design diagram and explain how workflows (use cases) will be fulfilled_

Digging deeper into the use cases, the following diagram describes the high level solution for requesting a ride.

In this case...

<img src="./image-1.png" alt="Component level"
	title="Request a Ride - Component Level Design" width="900" />

##### Brief explanation of diagram:
- User submits a request for a ride from their location (lat,longitude) to a destination (lat, long)
- The app server will process the request and send an event "ride_requested"
- LocateMe Service, once it sees the "ride_requested" event, LocateMe will talk to google S2 Library (https://developers.google.com/maps/documentation/gaming/concepts_playable_locations) to get the exact location using a flat view of the map with x,y coordinates and small boxes that delimit areas.
- Price Service.  Calculates the cost of the trip based on origin - destination. (using Dijkstras Algorithm )[https://stackabuse.com/dijkstras-algorithm-in-python]
- Ride_Dispatcher Service. Finds cabs in the proximity and sends the request.
- Cab_Request Service. Send messages to the top drivers near by and wait for confirmation.  Once confirmation is received, an event "Driver_Confirmed" is generated.
- Cab_Tracker Service. The service will maintain constant updates on the Cab location.
- The confirmation is then passed back to the Rider with all the details regarding the Cab.

##### Notes:
- In the design above there is no step for confirmation of the cost (needs to be added)
- In the design above there are many assumptions and details on how much the use case was simplified
- There is no consideration for car type, child seats, luggage capacity, or preferred customers

#### 5.1 Technology Details
- Web server and other points of access will have load balancers (LB) 
- All databases will have cache of some sort
- All the services will be run on Kubernetes/Docker for scaling

### 6. Faiure Modes / Risks
_What are the risks and potential issues to this design that need to be reviewed or solved. What are the conditions that will cause failures to the solution/system, and other risks identified._

- System crash will cause caos: riders don't know what to do (wait/go/call again), and drivers won't know where to go
- Network connectivity loss (e.g. rider is inside a building garage)
- Crash in the middle of transaction

### 7. External Interfaces
_Describe any requirements to external interfaces that the solution needs to interact with_

- Hardware Interfaces - N/A
- Software Interfaces - Google S2 APIs

### 8. Database Design
_Add the database design, Entity Relationship Diagram, Event formats, data types, and any detail you have_

### 9. USER / SYSTEM WORKFLOW DIAGRAMS
_Start describing individual Workflows for users, and inside the system.  Use UML diagrams and other tools that will help you make this very clear _

### 10. COMPONENTS / API (Brainstorm)
_In this section do a deeper dive into the components. Define the service, why it is needed, what it does, and how it will accomplish this function. Also add 
API definitions and contracts if you have them_

### 11. OPS READINESS / MONITORING
_Ready to go to production? describe your support plan including things as Support channels / pager duty / Prod monitoring / SLA_

