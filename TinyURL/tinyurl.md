# TinyURL - URL Shortening Service
The goal is to design a TinyURL like web service that will provide short aliases redirecting to long URLs. Short links save a lot of space when displayed, printed, messaged, or tweeted. Additionally, users are less likely to mistype shorter URLs.

## 1. What is TinyURL (Why - What - How)
TinyURL like service does the following:
* enable users to create short URLs aliases for Web Resources.
* Users can share these short URL and redirection to the original resource will happen.
* generates unique URLs or allow the creation of custom URLs. 

## 2. Defining Scope
### 2.1 Assumptions
* This is a read heavy system  1:10 proportion (write:read)
* The system will only allow users to generate up to 100 short URLs
* Registration/Account creation process will not be discussed
* custom URLs should not be longer than 16 characters
* Short-URLs are accessible to anybody (no protection)
* This solution does not support offline scenario

### 2.2 Users and Interactions
* URL owners will generate / assign short URLs for Web resources.  These users will register. 
* URL owners will be able to manage their URL list.  Add / Delete / Update / View access logs. 
* Users accessing short URLs will be automatically redirected to the long URL.  These users are not registered.
* No other type of user considered.

### 2.3 User Requirements
* After URL owners register, they will be allowed to create short-URL that can be used to access the original resources.
* URL owners need a way to manage their short-URLs and redirects.
* short-URLs will be redirected to the original web resource.
* short-URLs will have an expiration date (1 year) that the owner can reset.

After the URL owner registers for the service, they will be granted access to generate short-URLs.

### 2.4 Functional Requirements
* System needs to have a registration feature for profile management
* System must generate Unique URLs for each resource.
* If a custom URL is requested, the solution must verify that it is Unique
* System will redirect short-URLs to the original long-URL
* If a long-URL is not available, the system should fail gracefully with a custom error
* Every generated URL must be unique
* URLs and content should expire after 365 days
* The solution must provide insights and analytics through a dashboard
* The system should capture telemetry for users, usage, and content
* The solution should validate that the targetted resource does exist.

### 2.5 Non Functional Requirements
* The system should be highly available so that users can access their content anytime.
* The system should be highly reliable (URLs should not be lost)
* The system should meet low latency requirements so that interaction with users is highly responsive

### 2.6 Operating Environments
* Web browsers on desktop
* Web browsers on mobile devices (phones / tablets)
* No support for offline updates

### 2.7 Design Considerations
* The solution should use microservices
* There must be a REST api service to allow the same basic functions: upload, download, list content
* 

### 2.8 Capacity Considerations
From Assumptions:
*  Write:Read ratio - 1:5 
*  1M new objects per day
*  objects are in average 10Kb in size

**Calulations**:
* Write objects per second:  1M/(24 hours x 3600 sec) ~= 12 pastes per second
* Read objects per second:   5 x Write ~= 60 reads per second
* Capacity storage per day:  1M x 10kb = 10Gb per day
* Capacity storage for a year:  10Gb x 365 days = 3.6TB
* Storage for 10 years: 36TB
* Increase capacity to use up to 70% only:  36TB/.7 = 51.4TB

**Unique Keys**:
* We need unique keys for 36 Billion pastes in 10 years.
* If we use a 6 character key with base 64 encoding: 64^6 = 68.7 billion keys
* each key will be 6 bytes.  For 36 billion keys we need: 36B keys x 6 chars ~= 22GB (keys for 10 years)

**Cache**:
* Most used resources / keys will be cached.  Using 80/20 rule, where 20% of the objects will cause 80% of the traffic
* Total memory for cache:  5M reads x 10Kb x 20% ~= 10GB

### 3. Other Requirements
#### 3.1 Performance and Scalability
What Is the performance requirement (Low Latency, TPS, availability P99, etc.)
* Response time latency < 10ms

#### 3.2 Security
Security requirements.  Encryption / hashing / encoding / tokenization.
* N/A

#### 3.3 Authentication and Authorization
Login / profiles  and access controls
* User registration must be password protected
* Users should be able to login using email accounts (integration to Google/Hotmail/etc)
* Users should be able to create a custom account (not email integration)
* Passwords must be encrypted and not reversible

#### 3.4 Software Quality
What is the software quality requirement and how to measure it.  For example, bugs per 1000 lines, test results, integration, etc.

#### 3.5 Telemetry / Logging / Analytics
Requirements to measure user and system activities, event logging, and analytics

#### 3.6 Fault Tolerance
What is the expected fault tolerance? e.g. what happens if there is a crash in the middle of transaction
* Data should never be lost 
* If failure during the creation of URLs will need to be repeated

#### 3.7 Availability
What is the expected availability.  How are outages going to be handled.
* 24x7 - highly available 

#### 3.8 Monitoring and Alerting
* All systems logs go to DataDog
* All application logs go to Splunk
* Alerting will be handle through PagerDuty and Slack

#### 3.9 Database Storage / Partitioning | Sharding / Replication / Retention Policy 
##### 3.9.1 Storage
* We will store billions of records
* We store 2 types of data:  object and metadata
* Objects are less than 10 Mb
* Metatada is less than 1 K
* There is no relationship between records (except for user --> Paste-bins)
* 10 TB
##### 3.9.2 Replication
* Data should be replicated and available on multiple regions
##### 3.9.3 Retention Policy
* Data will be available for 10 days
* Users can make data availabel for a custom number of days between 1-30 days
##### 3.9.4 Partitioning | Sharding
* Data will be partitioned using userid  

### 4. High Level Design

<img src="./image.png" alt="high level"
	title="High Level Design" width="600" />

The assumption is that the user will talk to some appserver that will generate a new unique short URL for a given resource.  The metadata for those resources will be saved on a non SQL database.  Finally, the user will get a URL back.

### 5. Component Design
Going deeper into the solution we need to look at various components and how the data will flow through the system.

In this case...

<img src="./image-1.png" alt="Component level"
	title="Component Level Design" width="900" />

- The user requests a new short-URL through a web UI
- The appServer will generate a "URL Requested Event"
- The "URL Service" will request a new unique URL from the "Key Generation Service"
- The "Key Generation Service" will return a unique Id. If a custom URL is requested and it generates error (e.g. duplicate) the error will be passed back.
- The "Storage Service" will save the content and return the new short-URL
- "Key Generation Service" will move it to the used URL table. 
- The Key Generation Service will work on the background and maintain pre-generated / cache keys for usage.  
- Also, the Key Generation Service will manage and recycle expired Links:Keys


#### 5.1 Technology Details
- Web server and other points of access will have load balancers (LB) 
- All the services will be run on Kubernetes for scaling

### 6. Faiure Modes / Risks
_What are the risks and potential issues to this design that need to be reviewed or solved. What are the conditions that will cause failures to the solution/system, and other risks identified._

- Caching keys can cause loss of unused keys if the system is restarted
- The Key generation service can be improved to avoid single point of failure
- Expired keys need to be recycle
- Service not Available
- Crash in the middle of transactions
- Circular links (e.g. web crawler)

### 7. External Interfaces
_Describe any requirements to external interfaces that the solution needs to interact with_

- Hardware Interfaces / None
- Software Interfaces / None

### 8. Database Design
_Add the database design, Entity Relationship Diagram, Event formats, data types, and any detail you have_

### 9. USER / SYSTEM WORKFLOW DIAGRAMS
_Start describing individual Workflows for users, and inside the system.  Use UML diagrams and other tools that will help you make this very clear _
- Owner creates new short-URL
- Owner administers URL
- User access a expired short-URL
- User access a working short-URL

### 10. COMPONENTS / API (Brainstorm)
_In this section do a deeper dive into the components. Define the service, why it is needed, what it does, and how it will accomplish this function. Also add 
API definitions and contracts if you have them_
- Key Generation Service : Generates and maintain keys preemptively. Recycles keys once they expired.
- URL Service : handles URL requests from users
- Data Storage Service : manage the storage of URLs and any metada

### 11. OPS READINESS / MONITORING
_Ready to go to production? describe your support plan including things as Support channels / pager duty / Prod monitoring / SLA_

