# Paste Bin
The goal is to design a Pastebin like web service, where users can store plain text. Users of the service will enter a piece of text and get a randomly generated URL to access it.

## 1. What is PasteBin
Pastebin like services does the following:
* enable users to store plain text or images over the network (typically the Internet) 
* generate unique URLs to access the uploaded data. 
Such services are also used to share data over the network quickly, as users would just need to pass the URL to let other users see it.

## 2. Defining Scope
### 2.1 Assumptions
* The system will only allow users to upload files < 10 Mb
* Registration/Account creation process will not be discussed
* We expect 1M new objects pasted every day
* This is a read heavy system  1:5 proportion (write:read)

### 2.2 Users and Interactions
* Content owners who are loading content to be shared.  These users will register. 
* Content owners will be able to manage their content bins.  Add / Delete / Update / View access logs. 
* Consumers of content who will view and download the content.  These users are not registered.
* Other systems.  Will use an API to access the service.

### 2.3 User Requirements
* After users register for the service, they will be granted access to load text and files.
* Users will be able to generate a unique URL to share with others
* Users will be able to generate a custom URL to share with others
* The URL generated has to be unique and not predictable
* Users should be able to change the expiration date of their content
* No single user can have more than 10 Gb uploaded at a given time

### 2.4 Functional Requirements
* System needs to have a registration feature for profile management
* System must support files in various formats.  
* Files must be accessible to anybody with the access URL (no registration required)
* Every generated URL must be unique
* URLs and content should expire after 15 days
* The service must provide insights and analytics through a dashboard
* The system should capture telemetry for users, usage, and content

### 2.5 Non Functional Requirements
* The system should be highly available so that users can access their content anytime.
* The system should be highly reliable (uploaded content should not be lost)
* The system should meet low latency requirements so that interaction with users is highly responsive

### 2.6 Operating Environments
* Web browsers on desktop
* Web browsers on mobile devices (phones / tablets)

### 2.7 Design Considerations
* The solution should use microservices
* There must be a REST api service to allow the same basic functions: upload, download, list content
* 

### 2.8 Capacity Considerations
From Assumptions:
*  Write:Read ratio - 1:5 
*  1M new objects per day
*  objects are in average 10Kb in size

**Calulations**:
* Write objects per second:  1M/(24 hours x 3600 sec) ~= 12 pastes per second
* Read objects per second:   5 x Write ~= 60 reads per second
* Capacity storage per day:  1M x 10kb = 10Gb per day
* Capacity storage for a year:  10Gb x 365 days = 3.6TB
* Storage for 10 years: 36TB
* Increase capacity to use up to 70% only:  36TB/.7 = 51.4TB

**Unique Keys**:
* We need unique keys for 36 Billion pastes in 10 years.
* If we use a 6 character key with base 64 encoding: 64^6 = 68.7 billion keys
* each key will be 6 bytes.  For 36 billion keys we need: 36B keys x 6 chars ~= 22GB (keys for 10 years)

**Cache**:
* Most used resources / keys will be cached.  Using 80/20 rule, where 20% of the objects will cause 80% of the traffic
* Total memory for cache:  5M reads x 10Kb x 20% ~= 10GB

### 3. Other Requirements
#### 3.1 Performance and Scalability
What Is the performance requirement (Low Latency, TPS, availability P99, etc.)
* Response time latency < 10ms

#### 3.2 Security
Security requirements.  Encryption / hashing / encoding / tokenization 

#### 3.3 Authentication and Authorization
Login / profiles  and access controls

#### 3.4 Software Quality
What is the software quality requirement and how to measure it.  For example, bugs per 1000 lines, test results, integration, etc.

#### 3.5 Telemetry / Logging / Analytics
Requirements to measure user and system activities, event logging, and analytics

#### 3.6 Fault Tolerance
What is the expected fault tolerance? e.g. what happens if there is a crash in the middle of transaction
* Data is never lost 
* If failure during upload / download the operation will need to be repeated

#### 3.7 Availability
What is the expected availability.  How are outages going to be handled.
* 24x7 - highly available 

#### 3.8 Monitoring and Alerting
* All systems logs go to DataDog
* All application logs go to Splunk
* Alerting will be handle through PagerDuty and Slack

#### 3.9 Database Storage / Partitioning | Sharding / Replication / Retention Policy 
##### 3.9.1 Storage
* We will store billions of records
* We store 2 types of data:  object and metadata
* Objects are less than 10 Mb
* Metatada is less than 1 K
* There is no relationship between records (except for user --> Paste-bins)
* 10 TB
##### 3.9.2 Replication
* Data should be replicated and available on multiple regions
##### 3.9.3 Retention Policy
* Data will be available for 10 days
* Users can make data availabel for a custom number of days between 1-30 days
##### 3.9.4 Partitioning | Sharding
* Data will be partitioned using userid  

### 4. High Level Design
<img src="./image.png" alt="high level"
	title="High Level Design" width="600" />

The assumption is that the user will talk to some webserver that will send the objects to a repository (S3 or bigdata).  Then the metadata for that content will be extracted and saved for reference.  Finally, the user will get a URL back.

### 5. Component Design
Going deeper into the solution we need to look at various components and how the data will flow through the system.

In this case...
<img src="./image-2.png" alt="high level"
	title="High Level Design" width="900" />

- The user uploads a file through a web UI
- The file is passed to the webserver sends the file to the storage server
- storage server (SS) will save the content and return physical location
- SS sends an event "Content Uploaded" 
- The Metadata Service will see the SS event and update all metadata for the file (e.g. location: latitude/longitude/etc.)
- URL Service will see the SS event and retrieve the URL.  And move it to the used URL table. 
- The Key Generation Service will work on the background and maintain pre-generated / cache keys for usage

#### 5.1 Technology Details
- Web server and other points of access will have load balancers (LB) 
- All the services will be run on Kubernetes for scaling

